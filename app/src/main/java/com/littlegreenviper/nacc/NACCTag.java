package com.littlegreenviper.nacc;

/**************************************************************************************************/
/**
 This is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 NACC is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this code.  If not, see <http://www.gnu.org/licenses/>.package com.littlegreenviper.nacc;
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

/**************************************************************************************************/
/**
    This class is used to generate a tag image object.
    The class is instantiated with a period.
    It then determines which tag to use to represent that period.
 */
public class NACCTag {
    String      tagFileName = "";
    Activity    context = null;

    /**********************************************************************************************/
    /**
        Constructor. We figure out which tag to display here.

        @param inContext The main activity.
        @param inTotalDays The total number of days in the period represented by the tag (Ignored if over 6 months).
        @param inTotalMonths The total number of months in the period (ignored if 90 days or less).
     */
    public NACCTag ( Activity inContext, int inTotalDays, int inTotalMonths ) {
        this.context = inContext;

        if ( inTotalMonths > 3 ) {  // If we are counting months, then we ignore the total days.
            if ( (inTotalMonths > 2) && (inTotalMonths < 9) ) {
                this.tagFileName = "tag_05_layout";
            } else {
                if ( (inTotalMonths > 6) && (inTotalMonths < 12) ) {
                    this.tagFileName = "tag_06_layout";
                } else {
                    if ( (inTotalMonths > 9) && (inTotalMonths < 18) ) {
                        this.tagFileName = "tag_07_layout";
                    } else {
                        if ( (inTotalMonths > 12) && (inTotalMonths < 24) ) {
                            this.tagFileName = "tag_08_layout";
                        } else {
                            if ( (inTotalMonths > 18) && (inTotalMonths < 120) ) {
                                this.tagFileName = "tag_09_layout";
                            } else {
                                if ( (inTotalMonths > 24) && (inTotalMonths < 240) ) {
                                    this.tagFileName = "tag_10_layout";
                                } else {
                                    if ( (inTotalMonths > 120) && (inTotalMonths < 300) ) {
                                        this.tagFileName = "tag_11_layout";
                                    } else {
                                        this.tagFileName = "tag_12_layout";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if ( (inTotalDays > 0) && (inTotalDays < 30) ) {
                inTotalDays = 1;
                this.tagFileName = "tag_01_layout";
            } else {
                if ( (inTotalDays > 1) && (inTotalDays < 60) ) {
                    this.tagFileName = "tag_02_layout";
                } else {
                    if ( (inTotalDays > 30) && (inTotalDays < 90) ) {
                        this.tagFileName = "tag_03_layout";
                    } else {
                        this.tagFileName = "tag_04_layout";
                    }
                }
            }
        }
    }

    /**********************************************************************************************/
    /**
        Returns the tag as a LinearLayout.

        @returns a LinearLayout, with the tag image object embedded.
     */
    public LinearLayout getTagAsLayout () {
        int             layoutID = this.context.getResources().getIdentifier(this.tagFileName, "layout", this.context.getPackageName());
        LayoutInflater  inflater = this.context.getLayoutInflater();
        LinearLayout    ret = (LinearLayout) inflater.inflate(layoutID, null);
        return ret;
    }
}
