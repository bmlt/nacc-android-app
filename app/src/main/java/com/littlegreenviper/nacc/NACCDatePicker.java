package com.littlegreenviper.nacc;

/**************************************************************************************************/
/**
 This is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 NACC is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this code.  If not, see <http://www.gnu.org/licenses/>.package com.littlegreenviper.nacc;
 */

/**************************************************************************************************/

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import java.util.Calendar;


/**
    A very simple date picker dialog. Nothing fancy.
 */
public class NACCDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    /**********************************************************************************************/
    /**
        This method is called when the dialog is created.

        @param savedInstanceState The saved instance state (ignored).

        @returns a new dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    /**********************************************************************************************/
    /**
        This is called when the "OK" button is hit.

        @param view The DatePicker control view (ignored).
        @param year The year selected.
        @param month The month selectd (0-11).
        @param day The day of the month selected.
     */
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // We simply call our activity's handler.
        NACCActivity activity = (NACCActivity) getActivity();
        activity.calculateCleantime(year, month +1, day);
    }
}