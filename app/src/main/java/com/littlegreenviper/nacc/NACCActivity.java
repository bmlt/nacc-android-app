package com.littlegreenviper.nacc;

/**************************************************************************************************/
/**
 This is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 NACC is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this code.  If not, see <http://www.gnu.org/licenses/>.package com.littlegreenviper.nacc;
 */

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

/**************************************************************************************************/
/**
    The NACC is a very basic app that is a simple cleantime calculator. It's meant to be used by
    NA members as "entertainment." The member enters their cleandate, and the app reports how
    long they have been clean, in total days, as well as years, months and days.
    The NACC also displays a scrolling image that displays the standard (and some non-standard)
    NA keychains.

    This activity is the only activity for the NACC app.
 */
public class NACCActivity extends Activity {
    static int sRingBuffer  = 14;   // This is used to bring the rings to the proper location.

    /**********************************************************************************************/
    /**
        Simply sets the content view from our layout.

        @param savedInstanceState The instance state from the operating system. (ignored)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_nacc);
    }

    /**********************************************************************************************/
    /**
        When the app is sent to the background and brought forward again, we reset to an initial state.
     */
    @Override
    public void onResume() {
        super.onResume();

        RelativeLayout    tagDisplayView = (RelativeLayout) findViewById(R.id.tags_container);

        if(tagDisplayView.getChildCount() > 0) {
            tagDisplayView.removeAllViews();
        }

        TextView displayTextView = (TextView) findViewById(R.id.explain_text_view);

        displayTextView.setText(getString(R.string.explain_text));
    }

    /**********************************************************************************************/
    /**
        This displays a basic Date Picker dialog. This dialog is where the user selects their
        cleandate. They hit "OK" and the calculation commences.

        @param v The main view (ignored)
     */
    public void showDatePickerDialog(View v) {
        FragmentManager manager = getFragmentManager();
        DialogFragment newFragment = new NACCDatePicker();
        newFragment.show(manager, null);
    }

    /**********************************************************************************************/
    /**
        This is the method that actually performs the calculation. It takes in the date, as sent
        from the Date Picker dialog, and converts it into some plain English text, and triggers the
        display of the keytags.

        @param year The Gregorian year
        @param month The month (1-12).
        @param day The day of the month (0-31).
     */
    public void calculateCleantime(int year, int month, int day) {
        LocalDate   cleanDate = new LocalDate(year, month, day);

        int totalDays = Days.daysBetween (cleanDate, LocalDate.now()).getDays();

        // We only update if the result is nonzero.
        if ( totalDays != 0 ) {
            // Get the total number of years, months into the final year, and days into the final month.
            int years = new Period(new LocalDate(year, month, day), LocalDate.now(), PeriodType.yearMonthDay()).getYears();
            int months = new Period(new LocalDate(year, month, day), LocalDate.now(), PeriodType.yearMonthDay()).getMonths();
            int days = new Period(new LocalDate(year, month, day), LocalDate.now(), PeriodType.yearMonthDay()).getDays();

            // This generates a basic English date.
            DateTimeFormatter dateFormat = DateTimeFormat.forPattern("MMMM d, yyyy");

            // Create the simple "Your Clean Date Is" string.
            String cleanDateString = cleanDate.toString(dateFormat);

            // This gets the summary of the date string.
            String displayResult = this.getDisplayCleandate(totalDays, years, months, days, cleanDateString);

            // Place that into our main text area.
            TextView displayTextView = (TextView) findViewById(R.id.explain_text_view);

            displayTextView.setText(displayResult);

            // We will build an array of tag objects. The array is filled with how many the user should have.
            int             totalMonths = (years * 12) + months;
            List<NACCTag>   tags = new ArrayList<NACCTag>();

            // What we do here, is load up the array with tag objects, which we'll render, next.
            if ( totalMonths < 6 ) {
                totalMonths = 0;
            }

            if ( totalDays > 90 ) {
                totalDays = 90;
            }

            // The first four are "day count" tags.
            int elapsed = 1;
            while ( totalDays >= elapsed ) {
                tags.add(this.getTag(elapsed, 0));

                if ( elapsed == 1 ) {
                    elapsed = 30;
                } else {
                    if ( elapsed == 30 ) {
                        elapsed = 60;
                    } else {
                        if ( elapsed == 60 ) {
                            elapsed = 90;
                        } else {
                            break;
                        }
                    }
                }
            }

            // The next three are "month count" tags; including the on year tag.
            elapsed = 6;

            while ( totalMonths >= elapsed ) {
                tags.add(this.getTag(0, elapsed));

                if ( elapsed == 6 ) {
                    elapsed = 9;
                } else {
                    if ( elapsed == 9 ) {
                        elapsed = 12;
                    } else {
                        if ( elapsed == 12 ) {
                            break;
                        }
                    }
                }
            }

            // If we have 18 months or more, then we will continue adding tags.
            if ( totalMonths >= 18 ) {
                tags.add(this.getTag(0, 18));

                elapsed = 24;

                // We count through each year, and look for "special occasion" tags.
                while (totalMonths >= elapsed) {
                    if (elapsed == 120) {
                        tags.add(this.getTag(0, 120));
                    } else {
                        if (elapsed == 240) {
                            tags.add(this.getTag(0, 240));
                        } else {
                            if (elapsed == 300) {
                                tags.add(this.getTag(0, 300));
                            } else {
                                tags.add(this.getTag(0, 24));
                            }
                        }
                    }

                    elapsed += 12;
                }
            }

            // This is where the tags will be displayed.
            this.displayTags ( tags.toArray(new NACCTag[tags.size()]) );
        }
    }

    /**********************************************************************************************/
    /**
        This simply instantiates a tag object.

        @param inTotalDays The total number of days to be represented by the tag.
        @param inTotalMonths We may be looking at months, once past 90 days.

        @returns a new instance of NACCTag, set for the date given.
     */
    public NACCTag getTag(int inTotalDays, int inTotalMonths) {
        return new NACCTag( this, inTotalDays, inTotalMonths );
    }

    /**********************************************************************************************/
    /**
        This creates tag image objects, and fills the scroller with them.

        @param inTags This is an array of tag objects that we'll use to map the keychain chain.
     */
    public void displayTags ( NACCTag[] inTags ) {
        RelativeLayout    tagDisplayView = (RelativeLayout) findViewById(R.id.tags_container);

        // We will delete any previous tag objects.
        if(tagDisplayView.getChildCount() > 0) {
            tagDisplayView.removeAllViews();
        }

        // Get the main display sub-view. We use this to set the top of our tag list.
        // The tag list is under the main list in a relative layout, so we want to start the tags
        // a ways down, to make the text readable.
        View displaySpace = findViewById(R.id.tag_display_space);

        int offset = displaySpace.getTop();

        Drawable one_ring = ContextCompat.getDrawable(this, R.drawable.ring_01);

        int ring_offset = one_ring.getIntrinsicHeight() - sRingBuffer;

        // Create image views, and set them one over the other, offsetting to match the keyfob locations.
        for ( NACCTag tag: inTags ) {
            LinearLayout    tagView = tag.getTagAsLayout();

            RelativeLayout.LayoutParams rParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            offset += ring_offset;

            rParams.topMargin = offset;

            tagDisplayView.addView ( tagView, rParams );
        }
    }

    /**********************************************************************************************/
    /**
        This constructs and returns the plain English message that reports the cleantime.
        There are many choices for how it can be worded.

        @param inTotalDays The total number of days clean.
        @param inYears The number of years.
        @param inMonths The number of months into the final year.
        @param inDays The number of days into the final month.
        @param inDateFormatted This is the natural English date.

        @returns a string, to be displayed in the main area.
     */
    public String getDisplayCleandate(int inTotalDays, int inYears, int inMonths, int inDays, String inDateFormatted ) {
        // The first line is a simple report of the cleandate.
        String resultsString = String.format ( getString ( R.string.results_line1 ), inDateFormatted );

        // This is the second line, where we count days.
        if ( inTotalDays == 1 ) {  // Brand new
            resultsString += getString (R.string.results_day);
        }
        else if ( inTotalDays < 0 ) {
            resultsString = String.format(getString(R.string.results_line1_future), inDateFormatted) + getString (R.string.results_support);
        }
        else {
            resultsString += String.format(getString (R.string.results_days), inTotalDays);
        }

        // The next line is more involved, as it breaks into years, months and days.
        if ( inTotalDays > 90 ) {
            if ((inYears > 1) && (inMonths > 1) && (inDays > 1)) {
                resultsString += String.format(getString(R.string.results_complex_1), inYears, inMonths, inDays);
            } else if ((inYears > 1) && (inMonths > 1) && (inDays == 1)) {
                resultsString += String.format(getString(R.string.results_complex_2), inYears, inMonths);
            } else if ((inYears > 1) && (inMonths == 1) && (inDays == 1)) {
                resultsString += String.format(getString(R.string.results_complex_3), inYears);
            } else if ((inYears == 1) && (inMonths == 1) && (inDays == 1)) {
                resultsString += getString(R.string.results_complex_4);
            } else if ((inYears == 1) && (inMonths > 1) && (inDays == 0)) {
                resultsString += String.format(getString(R.string.results_complex_5), inMonths);
            } else if ((inYears == 1) && (inMonths == 1) && (inDays == 0)) {
                resultsString += getString(R.string.results_complex_6);
            } else if ((inYears == 1) && (inMonths == 0) && (inDays > 1)) {
                resultsString += String.format(getString(R.string.results_complex_7), inDays);
            } else if ((inYears == 1) && (inMonths == 0) && (inDays == 1)) {
                resultsString += getString(R.string.results_complex_8);
            } else if ((inYears > 1) && (inMonths > 1) && (inDays == 0)) {
                resultsString += String.format(getString(R.string.results_complex_9), inYears, inMonths);
            } else if ((inYears == 0) && (inMonths > 1) && (inDays > 1)) {
                resultsString += String.format(getString(R.string.results_complex_10), inMonths, inDays);
            } else if ((inYears == 0) && (inMonths > 1) && (inDays == 1)) {
                resultsString += String.format(getString(R.string.results_complex_11), inMonths);
            } else if ((inYears == 0) && (inMonths > 1) && (inDays == 0)) {
                resultsString += String.format(getString(R.string.results_complex_12), inMonths);
            } else if ((inYears > 1) && (inMonths == 0) && (inDays == 0)) {
                resultsString += String.format(getString(R.string.results_complex_13), inYears);
            } else if ((inYears > 1) && (inMonths == 1) && (inDays == 0)) {
                resultsString += String.format(getString(R.string.results_complex_14), inYears);
            } else if ((inYears == 1) && (inMonths > 1) && (inDays > 1)) {
                resultsString += String.format(getString(R.string.results_complex_15), inMonths, inDays);
            } else if ((inYears == 1) && (inMonths > 1) && (inDays == 1)) {
                resultsString += String.format(getString(R.string.results_complex_16), inMonths);
            } else if ((inYears == 0) && (inMonths == 1) && (inDays > 1)) {  // Should never happen.
                resultsString += String.format(getString(R.string.results_complex_17), inDays);
            } else if ((inYears == 0) && (inMonths == 1) && (inDays == 1)) {  // Should never happen.
                resultsString += getString(R.string.results_complex_18);
            } else if ((inYears > 1) && (inMonths == 0) && (inDays > 1)) {
                resultsString += String.format(getString(R.string.results_complex_19), inYears, inDays);
            } else if ((inYears > 1) && (inMonths == 0) && (inDays == 1)) {
                resultsString += String.format(getString(R.string.results_complex_20), inYears);
            } else if ((inYears == 1) && (inMonths == 0) && (inDays == 0)) {
                resultsString += getString(R.string.results_complex_21);
            } else if ((inYears > 1) && (inMonths == 1) && (inDays > 1)) {
                resultsString += String.format(getString(R.string.results_complex_22), inYears, inDays);
            } else if ((inYears == 1) && (inMonths == 1) && (inDays > 1)) {
                resultsString += String.format(getString(R.string.results_complex_23), inDays);
            } else {
                resultsString += getString(R.string.results_complex_24);
            }
        }

        return resultsString;
    }
}
